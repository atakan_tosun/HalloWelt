#include <math.h>
#include "ezs_io.h"
#include "ezs_test.h"

extern float sine_wave(const float frequency, const float amplitude, const float phase, const float y_offset, const unsigned int t_us);

void ezs_sanity_test(void) {
	unsigned failed = 0;
	const float test_threshold = 0.001f;
	extern const float ezs_res[256];
	float student_res;
	for(unsigned int i = 0; i < 256; i++) {
		student_res = sine_wave(10, 1, 0.25f, 0, i*1000);
		if (fabs(student_res - ezs_res[i]) >= test_threshold) {
			ezs_printf("Sanity test failed! Time: %u, expected: %f, got: %f\n", i*1000, ezs_res[i], student_res);
			failed++;
		}
	}
	if (!failed)
		ezs_printf("Sanity test successful\n");

	ezs_test_status_lcd(failed);
	while(1);
}
