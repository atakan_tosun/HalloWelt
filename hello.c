#include <cyg/hal/hal_arch.h>
#include <cyg/kernel/kapi.h>
#include <cyg/infra/diag.h>

#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <iso646.h>

#include "ezs_dac.h"
#include "ezs_gpio.h"
#include "ezs_delay.h"
#include "ezs_counter.h"
#include "ezs_io.h"
#include "ezs_serial.h"

#include "ezs_fb.h"
#include "ezs_lcd.h"

#include "colorcycling.h"

#ifdef SANITY_TEST
#include "ezs_test.h"
#endif

#define WHILE_LOOP_DELAY 1000000

/* Thread-Stack */
#define STACKSIZE CYGNUM_HAL_STACK_SIZE_MINIMUM + 2048
#define MY_PRIORITY 11
#define MY_PRIORITY_SIN 12
static cyg_uint8 my_stack[STACKSIZE];
static cyg_handle_t handle;
static cyg_thread threaddata;
static cyg_uint8 my_stack_sin[STACKSIZE];
static cyg_handle_t handle_sin;
static cyg_thread threaddata_sin;

// Image data for a colorcycling animation of the chair's logo.
#include "i4logo_animated.c"
extern image_t logo;
palette_t my_palette;

// Function prototypes
cyg_uint8 color_key_for_pixel(image_t *image, size_t col, size_t row);
color_t color_key_to_color(image_t *image, palette_t *palette, cyg_uint8 color_key);
void ezs_plot_animation(void);
float sine_wave(const float frequency, const float amplitude, const float phase, const float y_offset, const unsigned int t_us);
void test_thread(cyg_addrword_t arg);
void cyg_user_start(void);

// Function to extract color key for a pixel from an image
cyg_uint8 color_key_for_pixel(image_t *image, size_t col, size_t row) {
    int idx = ((row * image->width) + col) / 2;
    cyg_uint8 key = image->data[idx];

    if (idx % 2) {
        key = key << 4;
    }
    else{
        key = key & 15;
    }
    return key;
}

// Function to convert color key to color using a palette
color_t color_key_to_color(image_t *image, palette_t *palette, cyg_uint8 color_key) {
    cyg_uint8 lo = palette->mapping[color_key];
    return image->colors[lo];
}

// Function to plot animation on the LCD
void ezs_plot_animation(void) {
    pixel *const fb = ezs_lcd_get_fb();
    const size_t x_offset = 110;
    const size_t y_offset = 10;
    cyg_uint8 my_key;
    color_t my_color;
    for(int x = 0; x < logo.width; x++){
        for(int y = 0; y < logo.height; y++){
            my_key = color_key_for_pixel(&logo, x, y);
            my_color = color_key_to_color(&logo, &my_palette, my_key);
            ezs_lcd_set_pixel(fb, x + x_offset, y + y_offset, my_color.r, my_color.g, my_color.b);
        }
    }
    if(my_palette.next == NULL){
            my_palette = *_generate_palettes();
        }
    else{
        my_palette = *my_palette.next;
    }
}

// Function to compute the displacement of a sine wave at a given point in time
float sine_wave(const float frequency, const float amplitude, const float phase, const float y_offset, const unsigned int t_us) {
    // Convert time in microseconds to time in seconds
    float t_sec = t_us / 1000000.0f;

    // Calculate the angular frequency (2 * pi * frequency)
    float omega = 2.0f * M_PI * frequency;

    // Calculate the phase-shifted time
    float shifted_time = omega * t_sec + phase;

    // Calculate the sine wave displacement
    float displacement = amplitude * sinf(shifted_time);

    // Add the y-offset
    displacement += y_offset;
    
    return displacement;
}

// Test thread function
void test_thread(cyg_addrword_t arg) {
    ezs_lcd_init();
    my_palette = *_generate_palettes();
 // ezs_dac_init();
    while(1) {
        ezs_plot_animation();
        ezs_printf("Hallo Welt!\n");
 //  	ezs_dac_write(120);
        ezs_delay_us(WHILE_LOOP_DELAY);
    }
}

void sin_thread(cyg_addrword_t arg) {
    // Parameters for the sine wave
    float frequency = 1.0;    // Frequency in Hz
    float amplitude = 1.0;    // Amplitude of the wave
    float phase = 0.0;        // Phase shift in radians
    float y_offset = 0.0;     // Vertical offset
    uint8_t dac_value;
    ezs_dac_init();

    // Time parameters
    uint64_t t_us = 0;        // Time in microseconds
    float displacement_res;

    // Generate sine wave and print the result
    while(1){
        if(t_us == 1000000){
            t_us = 0;
        }
        t_us += 10000; 
        displacement_res = sine_wave(frequency, amplitude, phase, y_offset, t_us);
        dac_value = (uint8_t)((displacement_res + 1.0) * 127.5);
        ezs_dac_write(dac_value);
        ezs_delay_us(10000);
    }
    return;
}

// Initialization and start function
void cyg_user_start(void) {
    ezs_gpio_init();
    ezs_counter_init();
    ezs_serial_init();
    ezs_lcd_init();
    ezs_fb_init();
    ezs_dac_init();

#ifdef SANITY_TEST
    ezs_sanity_test();
#endif

    // Create and start the test thread
    cyg_thread_create(MY_PRIORITY, &test_thread, 0, "thread 1", my_stack, STACKSIZE, &handle, &threaddata);
    cyg_thread_create(MY_PRIORITY_SIN, &sin_thread, 0, "thread 2", my_stack_sin, STACKSIZE, &handle_sin, &threaddata_sin);
    cyg_thread_resume(handle);
    cyg_thread_resume(handle_sin);
}
