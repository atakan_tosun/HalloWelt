#include "ezs_io_fel.h"

#define EZSMAGIC "5c63"

typedef struct {
	int x;
	int y;
} cursor_t;

static cursor_t cursor;

/*!
 *  \brief Initialize the framebuffer. Call this function exactly once before
 *  using any of the other framebuffer related functions.
 */
void ezs_fb_init(void) {
	printf("%s:RPC:GUI:init()\n", EZSMAGIC);
}

cyg_ucount16 ezs_fb_width(void) {
	return 800;
}

cyg_ucount16 ezs_fb_height(void) {
	return 600;
}


/*!
 *  \brief Fill block.
 *  \param x X-Axis start position.
 *  \param y Y-Axis start position.
 *  \param width Width of the box.
 *  \param height Height of the box.
 *  \param color Color.
 */
void ezs_fb_fill_block(cyg_ucount16 x, cyg_ucount16 y, cyg_ucount16 width,
                       cyg_ucount16 height, cyg_fb_colour color) {
	printf("%s:RPC:GUI:fill_block(%u, %u, %u, %u, %u)\n", EZSMAGIC, x, y, width, height, color);
}

/*!
 *  \brief Draw horizontal line.
 *  \param x X-Axis start position.
 *  \param y Y-Axis start position.
 *  \param len length of the line.
 *  \param color Color.
 */
void ezs_fb_hline(cyg_ucount16 x, cyg_ucount16 y, cyg_ucount16 len,
                  cyg_fb_colour color) {
	printf("%s:RPC:GUI:hline(%u, %u, %u, %u)\n", EZSMAGIC, x, y, len, color);
}

/*!
 *  \brief Draw vertical line.
 *  \param x X-Axis start position.
 *  \param y Y-Axis start position.
 *  \param len length of the line.
 *  \param color Color.
 */
void ezs_fb_vline(cyg_ucount16 x, cyg_ucount16 y, cyg_ucount16 len,
                  cyg_fb_colour color) {
	printf("%s:RPC:GUI:vline(%u, %u, %u, %u)\n", EZSMAGIC, x, y, len, color);
}

/*!
 *  \brief Clear the framebuffer.
 *  \param color Color.
 */
void ezs_fb_clear(cyg_fb_colour color) {
	printf("%s:RPC:GUI:clear(%u)\n", EZSMAGIC, color);
}

/*  and so on.. see: http://ecos.sourceware.org/docs-latest/ref/framebuf-drawing.html   */

/*!
 * \brief Prints a character at a given position.
 * \param c Character to print.
 * \param x X-Axis position.
 * \param y Y-Axis position.
 * \param color Color.
 */
void ezs_fb_print_char(int c, cyg_ucount16 x, cyg_ucount16 y,
                       cyg_fb_colour color) {
	printf("%s:RPC:GUI:print_char(\"%c\", %u, %u, %u)\n", EZSMAGIC, c, x, y, color);
}

/*!
 * \brief Prints a string at a given position.
 * \param c Pointer to null terminated string.
 * \param x X-Axis position.
 * \param y Y-Axis position.
 * \param color Color.
 */
void ezs_fb_print_string(char* c, cyg_ucount16 x, cyg_ucount16 y,
                             cyg_fb_colour color) {
	// TODO: escaping for newlines here...
	printf("%s:RPC:GUI:print_string_cur(\"%s\", %u, %u, %u)\n", EZSMAGIC, c, x, y, color);

	size_t len = strlen(c);
	cursor.x = (x + 8 * len) % (FB_WIDTH - 8);
	cursor.y = y + 8 * len / FB_WIDTH;
}

void ezs_fb_print_string_at_cursor(char* c,  cyg_fb_colour color) {
	ezs_fb_print_string(c, cursor.x, cursor.y, color);
}

void ezs_fb_newline(void) {
	cursor.x = 0;
	cursor.y += 8;
}

void ezs_fb_setpos(cyg_ucount16 x, cyg_ucount16 y) {
	cursor.x = x;
	cursor.y = y;
}
