#include "ezs_utils.h"

#include <cyg/infra/diag.h>  // diag_printf
#include <cyg/kernel/kapi.h> // cyg_interrupt_disable

void die(const char *msg) {
	diag_printf("DIED: %s\n", msg);
	cyg_interrupt_disable();
	while(1);
}

