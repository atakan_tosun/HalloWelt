#include "ezs_deadlines.hpp"

#include <cyg/kernel/kapi.h>
#include "ezs_utils.h"

// Trust me, I'm an engineer. Don't do that at home
// Thread::thread_data is unfortunately private
#define private public
#include <cyg/kernel/thread.hxx>
#undef private

static cyg_ucount32 deadline_index = -1;
void ezs_deadline_init(void) {
	// Ecos allows for some thread specific data to be stored with a thread
	// However, this storage has to be allocated first, which is done here
	//
	// Storage for a tasks relative deadlines
	deadline_index = cyg_thread_new_data_index();
	if (deadline_index == (cyg_ucount32) -1) {
		die("Failed to alloc threadlocal storage for deadlines");
	}
}

static inline Cyg_Thread *handle_to_thread(cyg_handle_t thread) {
	// Yeah, this cast is ugly, and uses eCos internals, but this is how ecos code in
	// thread.inl does it
    return (Cyg_Thread *)thread;
}


void ezs_set_deadline(cyg_handle_t thread, cyg_addrword_t deadline) {
    Cyg_Thread *th = handle_to_thread(thread);

	th->thread_data[deadline_index] = deadline;
}


cyg_addrword_t ezs_get_deadline(cyg_handle_t thread) {
    Cyg_Thread *th = handle_to_thread(thread);

	return th->thread_data[deadline_index];
}
