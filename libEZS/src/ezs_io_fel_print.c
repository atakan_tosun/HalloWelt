#include "ezs_io_fel.h"

#define EZSMAGIC "5c63"
#define EZSINITMAGIC "1a44"

void ezs_fel_serial_init() {
	printf("%s:INIT\n", EZSINITMAGIC);
}

void ezs_printf(const char *fmt, ...) {
	printf("%s:MSG\n", EZSMAGIC);
	va_list args;
	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);
	printf("\n%s:ENDMSG:PTY\n", EZSMAGIC);
}

void ezs_log_file(const char *file, const char *fmt, ...) {
	printf("%s:MSG\n", EZSMAGIC);
	va_list args;
	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);
	printf("\n%s:ENDMSG:FILE:%s\n", EZSMAGIC, file);
}

