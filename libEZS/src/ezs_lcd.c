#include <stdio.h>
#include "libEZS/include/ezs_lcd.h"
#include "libEZS/font/font8x8_basic.h"
#include "libEZS/include/ezs_io.h"

typedef struct {
    int x;
    int y;
} cursor_t;

static cursor_t cursor;
static pixel *fb;

void ezs_fb_init(void) {
	fb = ezs_lcd_get_fb();
	ezs_fb_clear(CYG_FB_DEFAULT_PALETTE_WHITE);
    cursor.x = 0;
    cursor.y = 0;
}

cyg_ucount16 ezs_fb_width(void) {
	return LCD_WIDTH;
}

cyg_ucount16 ezs_fb_height(void) {
	return LCD_HEIGHT;
}

void ezs_fb_fill_block(cyg_ucount16 x, cyg_ucount16 y, cyg_ucount16 width,
                       cyg_ucount16 height, cyg_fb_colour color) {
	rgb_t c = ezs_color_to_rgb(color);
	size_t x_limit = (x + width) > FB_WIDTH ? FB_WIDTH : x + width;
	size_t y_limit = (y + height) > FB_HEIGHT ? FB_HEIGHT : y + height;
	for(size_t i = x; i < x_limit; i++) {
		for(size_t j = y; j < y_limit; j++) {
			ezs_lcd_set_pixel(fb, i, j, c.r, c.g, c.b);
		}
	}
}


void ezs_fb_hline(cyg_ucount16 x, cyg_ucount16 y, cyg_ucount16 len,
                  cyg_fb_colour color) {
	size_t limit = (x + len) > FB_WIDTH ? FB_WIDTH : x + len;
	rgb_t c = ezs_color_to_rgb(color);
	for(size_t i = x; i< limit; i++) {
		ezs_lcd_set_pixel(fb, i, y, c.r, c.g, c.b);
	}
}


void ezs_fb_vline(cyg_ucount16 x, cyg_ucount16 y, cyg_ucount16 len,
                  cyg_fb_colour color) {

	size_t limit = (y + len) > FB_HEIGHT ? FB_HEIGHT : y + len;
	rgb_t c = ezs_color_to_rgb(color);
	for(size_t i = y; i< limit; i++) {
		ezs_lcd_set_pixel(fb, x, i, c.r, c.g, c.b);
	}
}

void ezs_fb_clear(cyg_fb_colour color) {
    ezs_fb_fill_block(0, 0, FB_WIDTH, FB_HEIGHT, color);
}


void ezs_fb_print_char( int ch, cyg_ucount16 x, cyg_ucount16 y,
                        cyg_fb_colour color) {
	if (ch > 0x7f)
		ch = 0x7f;

	/* From libEZS/font/README:
	 * Every character in the font is encoded row-wise in 8 bytes.
	 * The least significant bit of each byte corresponds to the first pixel in
	 * a row.
	 */
    for (size_t i = 0; i < 8; i++) {
        unsigned char row = font8x8_basic[ch][i];

        for (size_t j = 0; j < 8; j++) {
            if (((row >> j) & 1) != 0) {
                ezs_fb_fill_block(x + j, y + i, 1, 1, color);
            }
        }
    }

}

void ezs_fb_print_string(char* c, cyg_ucount16 x, cyg_ucount16 y,
                             cyg_fb_colour color) {
    char *ch = c;

    while (*ch != '\0') {
        if (*ch == '\n') {
            y += 8;
			x = 0;
        } else {
            ezs_fb_print_char(*ch, x , y, color);
            x = x + 8;

            if ( x >= FB_WIDTH - 8) {
                y += 8;
                x = 0;
            }
        }

        ch++;
    }

    cursor.x = x;
    cursor.y = y;
}

void ezs_fb_print_string_at_cursor(char* c,  cyg_fb_colour color) {
    ezs_fb_print_string(c, cursor.x, cursor.y, color);
}

void ezs_fb_newline(void) {
    cursor.x = 0;
    cursor.y += 8;
}

void ezs_fb_setpos(cyg_ucount16 x, cyg_ucount16 y) {
    cursor.x = x;
    cursor.y = y;
}

