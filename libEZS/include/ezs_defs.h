#ifndef __EZS_DEFS_H__
#define __EZS_DEFS_H__

/**
 * \file ezs.h
 * \brief Common definitions for EZS files.
 */

#include <pkgconf/system.h>

#ifdef SMLPKG_TTKERNEL
#define __TT_ECOS (1)
#else
#define __TT_ECOS (0)
#endif

/** @brief Evaluates to `true` in the time-triggered variant of eCos. */
#define TT_ECOS __TT_ECOS

#endif // __EZS_DEFS_H__
