#ifndef __EZS_IO_FEL_H__
#define __EZS_IO_FEL_H__

/**
 * \file ezs_io_fel.h
 * \brief Helper functions to plot to the simulated framebuffer
 */


#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <cyg/infra/cyg_type.h>

#include "ezs_fb.h"

void ezs_fel_serial_init(void);

void ezs_printf(const char* fmt, ...)
#ifdef __GNUC__
          __attribute__ (( format( printf, 1, 2 ) ))
#endif
;

void ezs_log_file(const char* file, const char* fmt, ...)
#ifdef __GNUC__
          __attribute__ (( format( printf, 2, 3 ) ))
#endif
;
///////////////////// TERMINAL FOO //////////////////////////

#ifdef __cplusplus
}
#endif
#endif // __EZS_IO_FEL_H__
