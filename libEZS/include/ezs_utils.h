/*!
 * @file ezs_utils.h
 * @brief Misc utils for ecos development
 * @author Simon Schuster
 * \ingroup common
 * Some helper functions for general eCos development
 * \note You can still use the eCos functions directly, of course
 */

#ifndef __EZS_UTILS_H_
#define __EZS_UTILS_H_

#ifdef __cplusplus
extern "C"  {
#endif
	/*!
	 *  \brief Display an error message on serial and then loop endlessly.
	 *  \param msg The message to display
	 */
	void die(const char *msg) __attribute__((noreturn));
#ifdef __cplusplus
}
#endif

#endif
