/*!
 * @file ezs_test.h
 * @brief test helper
 * @author Phillip Raffeck
 * \ingroup test
 *
 */

#ifndef __EZS_TEST_H__
#define __EZS_TEST_H__

#include "ezs_lcd.h"

void ezs_sanity_test(void);
static inline void ezs_test_status_lcd(long long unsigned failed) {
	if (failed == 0) {
		ezs_fb_clear(CYG_FB_DEFAULT_PALETTE_GREEN);
		ezs_fb_print_string("ALL TESTS SUCCESSFUL!", 80, 102, CYG_FB_DEFAULT_PALETTE_BLACK);
		ezs_fb_print_string("You might be fine.", 80, 110, CYG_FB_DEFAULT_PALETTE_BLACK);
	} else {
		ezs_fb_clear(CYG_FB_DEFAULT_PALETTE_LIGHTRED);
		//ezs_fb_print_string("%llu TESTS FAILED.\nPlease check your implementation.\nSee serial output for more details.\n", (long long unsigned) ezs_failed_tests);
		ezs_fb_print_string("Some TESTS FAILED.", 80, 94, CYG_FB_DEFAULT_PALETTE_BLACK);
		ezs_fb_print_string("Please check your implementation.", 20, 102, CYG_FB_DEFAULT_PALETTE_BLACK);
		ezs_fb_print_string("See serial output for more details.", 20, 110, CYG_FB_DEFAULT_PALETTE_BLACK);
	}
}


#endif
