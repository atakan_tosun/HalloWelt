#ifndef EZS_ADC_H_INCLUDED
#define EZS_ADC_H_INCLUDED

/*!
 * @file ezs_adc.h
 * @brief A very simple AD converter, based on Fail*
 * @author Martin Hoffmann
 * \ingroup dac
 */

#include <stdint.h>

#include "ezs_sensor.h"

/**
 * \brief Returns the current value of the ezs ADC.
 * @return the value of the sensor.
 */
uint8_t ezs_adc_get(void);

/*!
 * \brief Get a value from the ADC.
 *
 * This is a synonym for ezs_adc_get();
 */
uint8_t ezs_adc_read(void);

#endif // EZS_ADC_H_INCLUDED
