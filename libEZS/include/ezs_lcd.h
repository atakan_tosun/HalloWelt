/*!
 * @file ezs_lcd.h
 * @brief Interface for the LCD.
 * @author Simon Schuster, Phillip Raffeck
 * \ingroup hardware
 */

#ifndef __LCD_H_
#define __LCD_H_

#include <libopencm3/stm32/ltdc.h>
#include "ezs_sdram.h"
#include "ezs_fb.h"

/*
 * NOTE: Dimensions switched!
 * Allows for better plotting and conformity to previous framebuffer.
 */
#define LCD_WIDTH  320
#define LCD_HEIGHT 240

typedef cyg_uint32 pixel;

/** Color encoding in RGB notation.*/
typedef struct rgb_color {
	cyg_uint8 r;
	cyg_uint8 g;
	cyg_uint8 b;
} rgb_t;

/**
 * \brief Returns a pointer to the beginning of the frame buffer of the LCD.
 *
 * Do not modify the memory directly, use ezs_lcd_set_pixel instead.
 */
static pixel* const ezs_lcd_get_fb(void);

/**
 * \brief Returns the height of the LCD screen.
 */
static const size_t ezs_lcd_height(void);

/**
 * \brief Returns the width of the LCD screen.
 */
static const size_t ezs_lcd_width(void);

/**
 * \brief Sets one pixel of the LCD.
 *
 * \param fb Pointer to begin of frame buffer, see ezs_lcd_get_fb().
 * \param x  x coordinate of the pixel to set.
 * \param y  y coordinate of the pixel to set.
 * \param r  Red value of the color of the pixel.
 * \param g  Green value of the color of the pixel.
 * \param b  Blue value of the color of the pixel.
 */
static void ezs_lcd_set_pixel(pixel *const fb, size_t x, size_t y, cyg_uint8 r, cyg_uint8 g, cyg_uint8 b);

/**
 * \fn rgb_t ezs_color_to_rgb(cyg_fb_colour color)
 * \brief Translates EZS/eCos color nanes to RGB values.
 *
 * Colors are chosen after `man colors` to match the behavior of the perl
 * framebuffer simulation.
 * 
 * \param color EZS/eCos color to translate.
 * \return RGB value correspoding to color.
 */
static rgb_t ezs_color_to_rgb(cyg_fb_colour color);

/** Necessary hardware initialization for LCD. */
int ezs_lcd_init(void);

/******************************* IMPLEMENTATION *******************************/

/** \cond IGNORE */
/* __attribute__ confuses Doxygen, so we hide them in an ignored section. */

static inline __attribute__((always_inline)) pixel* const ezs_lcd_get_fb(void) { return (pixel *)SDRAM_BASE_ADDRESS; }

static inline __attribute__((always_inline)) const size_t ezs_lcd_height(void) { return LCD_HEIGHT; }

static inline __attribute__((always_inline)) const size_t ezs_lcd_width(void)  { return LCD_WIDTH; }

static inline __attribute__((always_inline)) void ezs_lcd_set_pixel(pixel *const fb, size_t x, size_t y, cyg_uint8 r, cyg_uint8 g, cyg_uint8 b) {
	const cyg_uint8 alpha = 0xff;
	/* Note the switched x and y! */
	fb[x * LCD_HEIGHT + y] = (alpha << 24 | r << 16 | g << 8 | b);
}

static inline __attribute__((always_inline)) rgb_t ezs_color_to_rgb(cyg_fb_colour color) {
	switch(color) {
	case CYG_FB_DEFAULT_PALETTE_BLACK:
		return (rgb_t){0,0,0};
	case CYG_FB_DEFAULT_PALETTE_BLUE:
		return (rgb_t){0,0,139};
	case CYG_FB_DEFAULT_PALETTE_GREEN:
		return (rgb_t){0,100,0};
	case CYG_FB_DEFAULT_PALETTE_CYAN:
		return (rgb_t){0,139,139};
	case CYG_FB_DEFAULT_PALETTE_RED:
		return (rgb_t){139,0,0};
	case CYG_FB_DEFAULT_PALETTE_MAGENTA:
		return (rgb_t){139,0,139};
	case CYG_FB_DEFAULT_PALETTE_BROWN:
		return (rgb_t){165,42,42};
	case CYG_FB_DEFAULT_PALETTE_LIGHTGREY:
		return (rgb_t){211,211,211};
	case CYG_FB_DEFAULT_PALETTE_DARKGREY:
		return (rgb_t){169,169,169};
	case CYG_FB_DEFAULT_PALETTE_LIGHTBLUE:
		return (rgb_t){0,0,255};
	case CYG_FB_DEFAULT_PALETTE_LIGHTGREEN:
		return (rgb_t){0,255,0};
	case CYG_FB_DEFAULT_PALETTE_LIGHTCYAN:
		return (rgb_t){0,255,255};
	case CYG_FB_DEFAULT_PALETTE_LIGHTRED:
		return (rgb_t){255,0,0};
	case CYG_FB_DEFAULT_PALETTE_LIGHTMAGENTA:
		return (rgb_t){255,0,255};
	case CYG_FB_DEFAULT_PALETTE_YELLOW:
		return (rgb_t){255,255,0};
	case CYG_FB_DEFAULT_PALETTE_WHITE:
		return (rgb_t){255,255,255};
	}

	return (rgb_t){255,255,255};
}

/** \endcond */

#endif // __LCD_H_
