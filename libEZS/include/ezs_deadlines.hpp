/*!
 * @file ezs_deadlines.hpp
 * @brief Utils for storing deadline information with ecos tasks
 * @author Simon Schuster
 * \ingroup realtime
 * Some helper functions for manaaging deadline data.
 */

#ifndef __EZS_DEADLINES_H_
#define __EZS_DEADLINES_H_

#include <cyg/kernel/kapi.h>

#ifdef __cplusplus
extern "C"  {
#endif
	/*!
	 *  \brief Initialize handling of deadlines within the eCos kernel
	 *
	 *  \note Dies on error.
	 */
	void ezs_deadline_init(void);
	/*!
	 *  \brief associate a deadline value with a given thread specified by handle
	 *
	 *  \param thread The thread to attach the deadline value to
	 *  \param deadline The deadline value to set to the thread
	 */
	void ezs_set_deadline(cyg_handle_t thread, cyg_addrword_t deadline);
	/*!
	 *  \brief Retrieve a previously stored deadline value
	 *
	 *  \param thread The thread query the deadline value for
	 */
	cyg_addrword_t ezs_get_deadline(cyg_handle_t thread);
#ifdef __cplusplus
}
#endif


#endif
